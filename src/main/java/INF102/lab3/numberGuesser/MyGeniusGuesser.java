package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {


    @Override
    public int findNumber(RandomNumber number) {
        int left = number.getLowerbound();
        int right = number.getUpperbound();
        while ( left<=right){
            int mid = left + (right-left)/2;
            int numberGuess = number.guess(mid);
            if (numberGuess==0){
                return mid;
            }

            if (numberGuess==-1){
                left = mid;
            }
            else {
                right = mid;
                
                }
            }
        return left;
        }

    
}

   