package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        
            return peakElement(numbers, 0, numbers.size() -1);
        }
    
    private static int peakElement(List<Integer> nums, int left, int right) {
            if (left == right) {
                return nums.get(left);
            }
    
            int mid = left + (right - left) / 2;
    
            if (nums.get(mid) < nums.get(mid+1)) {
                return peakElement(nums, mid + 1, right);

            } else {
                return peakElement(nums, left, mid);
            }
        }

    }
